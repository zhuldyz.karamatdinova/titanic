import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    median_ages = {title: 0 for title in titles}
    missing_counts = {title: 0 for title in titles}
    for index, row in df.iterrows():
        name = row["Name"]
        age = row["Age"]
        title = None
        for word in name.split():
            if word.endswith("."):
                title = word
                break
        if title in titles:
            if not pd.isna(age):
                median_ages[title] += age
            else:
                missing_counts[title] += 1

    results = []
    for title in titles:
        median_age = round(median_ages[title] / (len(df) - missing_counts[title]))
        results.append((title, missing_counts[title], median_age))
    
    return results

filled_values = get_filled()
for result in filled_values:
    print(result)
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    return None
